package javaqplugin.weaver;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.LineNumberReader;
import java.io.PrintStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
 *
 * @author mdrzazga
 */
public class RabbitHoleWeaver {
    
    String currFile = "", currMethod = "", dslMeteClassName, rabbitHole;
    private List<String> asm, currMethodBody, dslInvoks;
    int currIndex = 0 , currMethodStart=0;
    
    public void go() {
        int i = 0;
        for (String pozycja : positions) {
            Triple info = parsePosition(pozycja);

            if (info != null) {
                if (!currFile.equalsIgnoreCase(info.compilationUnit)) {
                    emit();
                    changes = null;
                    currFile = info.compilationUnit;
                    System.err.println(currFile);
                    try {
                        asm = Files.readAllLines(getAsmFile(currFile).toPath());
                        
                    } catch (IOException ex) {
                        Logger.getLogger(RabbitHoleWeaver.class.getName()).log(Level.SEVERE, null, ex);
                    }

                }

                if (asm != null && !currMethod.equalsIgnoreCase(info.path)) {
                    currMethod = info.path;
                    wave(0);
                    currMethodStart=findStartMethod(info.path);
                    currMethodBody = getMethodBody(currMethodStart);
                    currIndex = i;
                }

            }
            i++;
               wave(0);
        }
     
        emit();
    }
    List<String> positions;
  

    public List<String> getPositions() {
        return positions;
    }

    public void setPositions(List<String> positions) {
        this.positions = positions;
    }

   

    static final Pattern PATH_PATTERN = Pattern.compile("(.*)@(.*)/(.*)/$");

    public Triple parsePosition(String line) {
        Triple ret = null;
        Matcher m = PATH_PATTERN.matcher(line);
        //TODO zrobic inner classy - przesuniecie danych z g2 do g3
        if (m.matches()) {
            ret = new Triple(m.group(1), m.group(3), m.group(2));
        }
        return ret;
    }
    
    public String getCompilationUnit(String position){
        Triple t = parsePosition(position);
        return (t!=null)?t.compilationUnit:null;
    }

    public static final Pattern METHOD_HEAD = Pattern.compile("^(?:public |public static |public varargs |public static varargs |"
            + "private |private static |private varargs |private static varargs |"
            + "static |static varargs |varargs |)Method (.*)$");
    public static final Pattern METHOD_DECLARATION = Pattern.compile("(.*):\"\\((.*)\\).*\"");

    public int findStartMethod(String methoInfo) {
        int ret = 0;
        for (String line : asm) {
            Matcher m = METHOD_HEAD.matcher(line);
            if (m.matches()) {
               
                String cMethod = m.group(1);
   
                m = METHOD_DECLARATION.matcher(cMethod);

                if (m.matches() && declarationMatches(methoInfo, m.group(1), m.group(2))) {

            //        System.out.println("pared: " + line + " : " + methoInfo);
                    return ret;
                }

            }
            ret++;
        }
        return 0;
    }

    public static final Pattern DSL_METHOD_REF = Pattern.compile("(.*)\\((.*)\\)");

    public static boolean declarationMatches(String methodInfo, String name, String args) {
        Matcher m = DSL_METHOD_REF.matcher(methodInfo);
        if (m.matches()) {
            if (name.equals(m.group(1))) {
                if (args.isEmpty() && m.group(2).length() == 0) {
                    return true;
                }
                String[] refArgs = m.group(2).split(",");
                StringBuilder sb = new StringBuilder();
                for (String a : refArgs) {
                    // replace varargs with array declaration
                    a = a.replace("...", "[]");
                    // point array dimension if any
                    int brackets = 0;
                    for (int i = 0; i < a.length(); i++) {
                        if (a.charAt(i) == '[') {
                            sb.append("[");
                            brackets++;
                        }
                    }
                    // remove array declartations []
                    a = a.substring(0, a.length() - 2 * brackets);
                    switch (a) {
                        case "int":
                            sb.append('I');
                            break;
                        case "long":
                            sb.append('J');
                            break;
                        case "short":
                            sb.append('S');
                            break;
                        case "byte":
                            sb.append('B');
                            break;
                        case "float":
                            sb.append('F');
                            break;
                        case "double":
                            sb.append('D');
                            break;
                        default:
                            sb.append("L").append(a.replaceAll("\\.", "/")).append(";");

                    }
                }

                return (args.isEmpty() && sb.toString().isEmpty()) || args.equals(sb.toString());
            }

        }
        return false;
    }

    private List<String> getMethodBody(int findStartMethod) {
        List<String> ret = new ArrayList<>();
        for (String line : asm.subList(findStartMethod, asm.size())) {
            ret.add(line);
            if (line.equals("}")) {
                break;
            }
        }
        System.err.println("method at: "+ findStartMethod);
        return ret;
    }

    public static Pattern DSL_INVOCATION = Pattern.compile(""
            + "		(?:invokevirtual|invokestatic)	Method (.*):\"\\((.*)\\).*\";");

    /**
     *
     * @param currMethodBody
     */
    private void wave(int startNr) {
//        System.out.println("idx: " + currIndex + "  " + positions.get(currIndex) + " []" + dlsArgsLenths.get(currIndex));
        Triple positionInfo = parsePosition(positions.get(currIndex));
        String line1 = "", line2 = "", line3 = "";
        int lastArrayLineNr = 0;
        if (currMethodBody != null) {
            int lineNr = startNr;
            for (String s : currMethodBody.subList(startNr, currMethodBody.size())) {
       //         System.out.println("[" + lineNr + "] " + s);
                line1 = line2;
                line2 = line3;
                line3 = s;

                Matcher m = DSL_INVOCATION.matcher(s);
                if (m.matches() && declarationMatches(positionInfo.dslMetjod, m.group(1).replaceAll("/", "\\."), m.group(2))) {
                    boolean isDslInvoke = declarationMatches(positionInfo.dslMetjod, m.group(1).replaceAll("/", "\\."), m.group(2));
               //     System.out.println("invocation maches at " + lineNr + " is DslInvoke " + isDslInvoke);
                    preapareModification(lastArrayLineNr, lineNr);
                    //procced to next DSL invocation
                    if (currIndex < positions.size() - 1) {
                        Triple nextInfo = parsePosition(positions.get(currIndex + 1));
                        if (positionInfo.path.equals(nextInfo.path)) {
                            currIndex++;
                            wave(lineNr + 1);
                            break;
                        }
                    }

                }

                lineNr++;
            }
        }

        currMethodBody = null;
    }

    /**
     * Method emits modificated jasm code to [oryginal_asam].gen file only if any
     * modifications have occured
     */
    private void emit() {
        if (changes == null || changes.isEmpty()) {
            return;
        }
        File outFile = getAsmFile(currFile + "_gen");
        int injectPos = asm.size() + 1;
        Tuple inject = null;
        for (Tuple k : changes.keySet()) {
            if (k.start < injectPos) {
                injectPos = k.start;
                inject = k;
            }
        }
       
        try {
            PrintStream out = new PrintStream(outFile);
            int p = 0;
            Iterator<String> asmIter = asm.iterator();
            while (asmIter.hasNext()) {
                if (p++ < injectPos) {
                    out.println(asmIter.next());
                } else {
                    // emit change
                    for (String l : changes.get(inject)) {
                        out.println(l);
                    }
                    // skip oryginal line
                   for (int skip = inject.start; skip < inject.end +1 ; skip++) {
                        if(asmIter.hasNext())asmIter.next();
                        p++;
                    }
                   if(asmIter.hasNext())out.println(asmIter.next());
                    // set new inject
              //      System.err.println("Code modified at "+ inject );
                   changes.remove(inject);
                   injectPos = asm.size() + 1;
                    for (Tuple k : changes.keySet()) {
                        if (k.start < injectPos) {
                            injectPos = k.start;
                            inject = k;
                 //           System.err.println("Insert for Injection "+ inject );
                        }
                    }
                   
                }
            }
            out.flush();

        } catch (Exception ex) {
            Logger.getLogger(RabbitHoleWeaver.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

  

    File asmDirectory;
    private File getAsmFile(String currFile) {
        File ret = new File(asmDirectory + "/" + currFile.replaceAll("\\.", "/") + ".jasm");
        //("plik: " +ret);
        return ret;
    }

    public static class Tuple {

        public int start, end;

        public Tuple(int start, int end) {
            this.start = start;
            this.end = end;
        }

        @Override
        public String toString() {
            return "Tuple{" + "start=" + start + ", end=" + end + '}';
        }

    }
    private Map<Tuple, List<String>> changes;

    /**
     * actualy modify jasm code to inject dsl reference and store modifacated
     * code in to changes map
     *
     * @param lastArrayLineNr
     * @param lineNr
     */
    private void preapareModification(int lastArrayLineNr, int lineNr) {
        if (changes == null) {
            changes = new HashMap<>();
        }
        List<String> modifiedCode = new ArrayList<>();
        
        
            //RABBIT HOLE
                modifiedCode.add(currMethodBody.get(lineNr-1));
                modifiedCode.add("		getstatic	Field " + dslMeteClassName + ".REF_" + currIndex + ":\"Ljava/util/List;\";");
                modifiedCode.add("		invokestatic	Method "+rabbitHole+":\"(Ljava/util/List;)V\";");
                changes.put(new Tuple(currMethodStart+ lineNr-1, currMethodStart+ lineNr - 1), modifiedCode);
        
    }

    static String asmIntValue(int size) {
        return "		" + ((size >= 0 && size < 6) ? "iconst_" + size : "bipush	" + size) + ";";
    }

    public String getDslMeteClassName() {
        return dslMeteClassName;
    }

    public void setDslMeteClassName(String dslMeteClassName) {
        this.dslMeteClassName = dslMeteClassName;
    }

    public String getRabbitHole() {
        return rabbitHole;
    }

    public void setRabbitHole(String rabbitHole) {
        this.rabbitHole = rabbitHole;
    }

    public File getAsmDirectory() {
        return asmDirectory;
    }

    public void setAsmDirectory(File asmDirectory) {
        this.asmDirectory = asmDirectory;
    }
    

    public class Triple {

        public String dslMetjod, compilationUnit, path;

        public Triple(String dslMetjod, String compilationUnit, String path) {
            this.dslMetjod = dslMetjod;
            this.compilationUnit = compilationUnit;
            this.path = path;
        }
    }

}
