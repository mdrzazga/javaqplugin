/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * test branczowania gita
 * and open the template in the editor.
 */
package javaqplugin;

import java.io.File;

/**
 *use
 * args[0] asm dir
 * args[1] class dir
 * args[2] metadata package
 * args[3] metadata simple classname
 * args[4] rabbit hole method name
 * @author mdrzazga
 */
public class Javaq {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
       
       MakeDSLTask task = new MakeDSLTask();
       task.setAsmDir(new File(args[0]));
       task.setClassDir(new File(args[1]));
       task.setMetaPackage(args[2]);
       task.setMetaClassName(args[3]);
       task.setRabbitHoleName(args[4]);
       task.execute();
    }
    
}
