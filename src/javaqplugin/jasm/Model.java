package javaqplugin.jasm;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author mdrzazga
 */
public class Model {
    final String position;
    final List<Triple> parameters;
    final StringBuilder dslCode;

    public Model(String position) {
        this.position = position;
        parameters = new ArrayList<>();
        dslCode = new StringBuilder();
    }

    public String getPosition() {
        return position;
    }

    public List<Triple> getParameters() {
        return parameters;
    }

    public String getDslCode() {
        return dslCode.toString();
    }
    boolean firstLine =true;
    public void addDSLLine(CharSequence line){
        if(line!=null){
            dslCode.append((firstLine)?"":"\\n").append(line);
            firstLine=false;
        }
    }

    @Override
    public String toString() {
        return "Model{" + "position=" + position + ", parameters=" + parameters + ", dslCode=" + dslCode + '}';
    }
    
    
    public static class Triple{
        public final String className;
        public final String name;
        public final String genericInfo;

        public Triple(String name, String clazz,  String genericInfo) {
            this.className = clazz;
            this.name = name;
            this.genericInfo = genericInfo;
        }     

        @Override
        public String toString() {
            return "Triple{" + "className=" + className + ", name=" + name + ", genericInfo=" + genericInfo + '}';
        }
        
        
    }
}
 
