package javaqplugin.jasm;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.LineNumberReader;
import java.io.Reader;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * parsuje tekst zwracany przez FindDSLVisitor
 *
 * @author mdrzazga
 */
public class MetaInfoParser {
    
     public static final String MAGIC = "BABECAFE";

    final InputStream is;
    String position;
    Pattern dslCodePattern = Pattern.compile(MAGIC + "(.*)");
    final static Pattern IDENTIFIER_PATTERN = Pattern.compile("IDENTIFIER:(.*):(.*)");
    final static Pattern GENERIC_TYPE = Pattern.compile("(.*)(<.*>)");
    LineNumberReader lr;

    public MetaInfoParser(InputStream is) {
        this.is = is;
        lr = new LineNumberReader(new InputStreamReader(is));
    }
    boolean ended = false;

    /**
     *
     */
    public enum STATE {

        IN_POSSITION, IN_PARAMETER, IN_DSL_CODE
    }

    /**
     * synchronicznie (blokuje) czyta is, parsuje i zwraca najblizszy Model lub
     * null jezeli koniec is lub blad parsowania po pierwszym null zwraca zawsze
     * null
     *
     * @return
     */
    public Model next() {
        if (ended) {
            return null;
        }
        try {
            String line = lr.readLine();
            Model ret = new Model(line);
            STATE state = STATE.IN_POSSITION;
            String magicReplacer = null;
            line = lr.readLine();
            while (line != null) {
                Matcher magicMacher = dslCodePattern.matcher(line);
                switch (state) {
                    case IN_DSL_CODE:
                        if (magicMacher.matches()) {
                            return ret;
                        } else {
                            if(magicReplacer!=null) line= line.replaceAll(magicReplacer, MAGIC);
                            ret.addDSLLine(line);
                        }
                    break;
                    default:
                        if(magicMacher.matches()){
                            state=STATE.IN_DSL_CODE;
                            magicReplacer = magicMacher.group(1);
                            if("".equals(magicReplacer))magicReplacer=null;
                        }else{
                            Matcher identifierM = IDENTIFIER_PATTERN.matcher(line);
                            if(identifierM.matches()){
                                String nazwa = identifierM.group(1);
                                String className = identifierM.group(2);
                                String genericInfo = null;
                                Matcher genM = GENERIC_TYPE.matcher(className);
                                if(genM.matches()){
                                    className=genM.group(1);
                                    genericInfo=genM.group(2);
                                }
                                ret.getParameters().add(new Model.Triple(nazwa, className, genericInfo));
                            }else{
                            //TODO ZROBIC LITRALY - ma snens ?
                                ret.getParameters().add(new Model.Triple(null, null, null));
                            }
                            
                        }
                }
                line= lr.readLine();
            }
        } catch (Exception ex) {
            Logger.getLogger(MetaInfoParser.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (is != null) {
                try {
             //       is.close();
                } catch (Exception ee) {
                }
            }
        }
        ended = true;
        return null;
    }

}
