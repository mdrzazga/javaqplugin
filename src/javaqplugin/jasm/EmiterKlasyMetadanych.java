package javaqplugin.jasm;

import java.io.PrintStream;
import java.util.List;

/**
 * klasa na podstawie zebranych metadanych emituje kod jasm dla klasy z
 * referencjami metadanych
 *
 * @author mdrzazga
 */
public class EmiterKlasyMetadanych {

    /**
     * produkuje kod ASM dla klasy meteadnaych
     *
     * @param out
     * @param data - metadane
     */
    public void createAsmCode(PrintStream out, List<Model> data) {
        int dataSize = data.size();
        out.println(naglowek);
        //REF_x declarations
        for (int i = 0; i < dataSize; i++) {
            out.println(declareREF_(i));
        }
        out.println(REFERENCES);
        out.println();
        out.println(CONSTRUKTOR);
        out.println();
        out.println(STATIC_BODY_START);
        int fildIdx = 0;
        int idx = 0;
        for (Model m : data) {
            List<Model.Triple> params = m.getParameters();
            //obejmuje przypadek pustego wywolania
            out.println(arraySerialisableDeclaration((params.size()) * 2 + 2/* genInfo +2*/));
            StringBuilder genInfo = new StringBuilder();
            idx = 0;
            for (Model.Triple p : params) {
                if (p.name != null) {
                    out.println(addStringLiteralToArray(idx, p.name));
                } else {
                    out.println(addNullToArray(idx));
                }
                idx++;
                if (p.className != null) {
                    out.println(addClassToArray(idx, cannonClassName(p.className)));
                } else {
                    out.println(addNullToArray(idx));
                }
                idx++;
                if (p.genericInfo != null) {
                    genInfo.append(idx / 2 - 1).append(':').append(p.genericInfo).append(';');
                }
            }
            //genericInfo
            // idx++;
            if (genInfo.length() > 0) {
                out.println(addStringLiteralToArray(idx, genInfo));
            } else {
                out.println(addNullToArray(idx));
            }
            idx++;
            if (m.dslCode != null) {
                out.println(addStringLiteralToArray(idx, m.dslCode));
            } else {
                out.println(addNullToArray(idx));
            }
            out.println(setRefFild(fildIdx));
            fildIdx++;
        }
        // REFERENECES ARRAY
        out.println(arrayStringDeclaration(dataSize));
        idx = 0;
        for (Model m : data) {
            out.println(addStringLiteralToArray(idx++, m.position));
        }
        out.println(STATIC_BODY_END);
        out.println(stopka);
        out.flush();

    }

    final String package_name, class_name, version, naglowek, stopka;
    final static String REFERENCES
            = "public static final Field REFERENCES:\"Ljava/util/List;\";",
            CONSTRUKTOR
            = "private Method \"<init>\":\"()V\"\n"
            + "	stack 1 locals 1\n"
            + "{\n"
            + "		aload_0;\n"
            + "		invokespecial	Method java/lang/Object.\"<init>\":\"()V\";\n"
            + "		return;\n"
            + "	\n"
            + "}",
            STATIC_BODY_START
            = "static Method \"<clinit>\":\"()V\"\n"
            + "	stack 4 locals 0\n"
            + "{",
            STATIC_BODY_END
            = "		invokestatic	Method java/util/Arrays.asList:\"([Ljava/lang/Object;)Ljava/util/List;\";\n"
            + "		invokestatic	Method java/util/Collections.unmodifiableList:\"(Ljava/util/List;)Ljava/util/List;\";\n"
            + "		putstatic	Field REFERENCES:\"Ljava/util/List;\";\n"
            + "		return;\n"
            + "}\n";

    /**
     *
     * @param package_name
     * @param class_name
     * @param version
     */
    public EmiterKlasyMetadanych(String package_name, String class_name, String version) {
        this.package_name = package_name;
        this.class_name = class_name;
        this.version = version;
        naglowek
                = "package  " + package_name + ";\n"
                + "\n"
                + "super public final class " + class_name + "\n"
                + "	version " + version + "\n"
                + "{\n"
                + "";
        stopka
                = "} // end Class " + class_name;

    }

    /**
     * deklaracja stalej referencji o zadanym indekcie
     *
     * @param idx
     * @return
     */
    static String declareREF_(int idx) {
        return "public static final Field REF_" + idx + ":\"Ljava/util/List;\";";
    }

    /**
     *
     * @param size
     * @return
     */
    static String arraySerialisableDeclaration(int size) {
        return "		" + asmIntValue(size) + ";\n"
                + "		anewarray	class java/io/Serializable;";
    }

    /**
     *
     * @param idx
     * @param className - nazwa klasy w formacie java/lang/Object
     * @return
     */
    static String addClassToArray(int idx, String className) {
        return "		dup;\n"
                + "		" + asmIntValue(idx) + ";\n"
                + "		ldc	class " + className + ";\n"
                + "		aastore;";
    }

    /**
     * //TODO w literal trzeba przekodowac znaki unikode na cody \\uXXXX
     *
     * @param idx
     * @param literal
     * @return
     */
    static String addStringLiteralToArray(int idx, CharSequence literal) {
       
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < literal.length(); i++) {
            char c = literal.charAt(i);
            if (c <= 0x7F) {
                sb.append(c);
            } else {
                sb.append("\\u");
                String hex = Integer.toHexString(c).toUpperCase();
                if (hex.length() == 2) {
                    sb.append("00");
                } else {
                    sb.append("0");
                }
                sb.append(hex);
            }
        }
        String stringLiteral = sb.toString();
        return "		dup;\n"
                + "		" + asmIntValue(idx) + ";\n"
                + "		ldc	String \"" + stringLiteral + "\";\n"
                + "		aastore;";
    }

    /**
     * dodaje null do tablicy
     *
     * @param idx
     * @return
     */
    static String addNullToArray(int idx) {
        return "		dup;\n"
                + "		" + asmIntValue(idx) + ";\n"
                + "		aconst_null;\n"
                + "		aastore;";
    }

    /**
     *
     * @param idx - index referencji
     * @return
     */
    static String setRefFild(int idx) {
        return "		invokestatic	Method java/util/Arrays.asList:\"([Ljava/lang/Object;)Ljava/util/List;\";\n"
                + "		invokestatic	Method java/util/Collections.unmodifiableList:\"(Ljava/util/List;)Ljava/util/List;\";\n"
                + "		putstatic	Field REF_" + idx + ":\"Ljava/util/List;\";";
    }

    /**
     *
     * @param size
     * @return
     */
    static String arrayStringDeclaration(int size) {
        return "		" + asmIntValue(size) + ";\n"
                + "		anewarray	class java/lang/String;";
    }

    /**
     * iconct_size (dla 0 <= size < 6) lub bipush size @param size
     *
     * @
     * return
     */
    static String asmIntValue(int size) {
        return (size >= 0 && size < 6) ? "iconst_" + size : "bipush	" + size;
    }

    /**
     * zwraca nazwe klasy w formacie wymaganym przez jasm (zgodne ze
     * specyfikacja)
     *
     * @param className
     * @return
     */
    static String cannonClassName(String className) {
        if (className.contains("[")) {
            int count = 0, firstOccur = 0;
            for (char c : className.toCharArray()) {
                if (c == '[') {
                    count++;
                }
            }
            firstOccur = className.indexOf('[');
            StringBuilder sb = new StringBuilder("\"");
            for (int i = 0; i < count; i++) {
                sb.append("[");
            }
            sb.append('L').append(className.substring(0, firstOccur).replaceAll("\\.", "/")).append(";\"");
            return sb.toString();
        } else {
            // autoboxing
            switch (className) {
                case "int":
                    return "java/lang/Integer";
                case "double":
                    return "java/lang/Double";
                case "float":
                    return "java/lang/Float";
                case "long":
                    return "java/lang/Long";
                case "boolean":
                    return "java/lang/Boolean";
                case "byte":
                    return "java/lang/Byte";
                case "char":
                    return "java/lang/Character";
                case "short":
                    return "java/lang/Short";

            }
            return className.replaceAll("\\.", "/");
        }
    }

}
