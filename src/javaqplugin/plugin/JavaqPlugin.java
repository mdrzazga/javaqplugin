package javaqplugin.plugin;


import com.sun.source.util.JavacTask;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author mdrzazga
 */
public class JavaqPlugin implements com.sun.source.util.Plugin {

    @Override
    public String getName() {
        return "JavaqPlugin";
    }

    @Override
    public void init(JavacTask jt, String... args) {
        StringBuilder sb = new StringBuilder();
         String projectKey = "" ;
        if(args.length >0) {
            projectKey =args[0];
            args = Arrays.copyOfRange(args, 1, args.length);
        }
     //   System.err.println("JavaqPlugin "+ projectKey);
        String asmDirPath = projectKey;
        System.err.println("JavaqPlugin "+ projectKey);
        File asmDir = new File(asmDirPath+"/metadata.txt");
        jt.addTaskListener(new CallDSLTaskListener(jt, args, asmDir));
    }

}
