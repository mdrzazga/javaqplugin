package javaqplugin.plugin;


import com.sun.source.tree.CompilationUnitTree;
import com.sun.source.util.JavacTask;
import com.sun.source.util.TaskEvent;
import com.sun.source.util.TaskListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.lang.model.element.TypeElement;

/**
 *
 * @author mdrzazga
 */
class CallDSLTaskListener implements TaskListener {

    FindDSLVisitor visitor;

    CallDSLTaskListener(JavacTask jt, String[] klasyKontraktu, File metaDataFile) {
        //TODO odszyc byle jak ale odszyć !!!
        PrintStream mout = null;
        try {
            mout= new PrintStream(metaDataFile);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(CallDSLTaskListener.class.getName()).log(Level.SEVERE, null, ex);
        }
        visitor= new FindDSLVisitor(jt,mout, klasyKontraktu);
    }

    @Override
    public void started(TaskEvent te) {
    }

    @Override
    public void finished(TaskEvent taskEvent) {
        if (taskEvent.getKind().equals(TaskEvent.Kind.ANALYZE)) {
            CompilationUnitTree compilationUnit = taskEvent.getCompilationUnit();
            visitor.scan(compilationUnit, null);
        }
    }
    
}
