package javaqplugin.plugin;


import com.sun.source.tree.BinaryTree;
import com.sun.source.tree.BlockTree;
import com.sun.source.tree.ClassTree;
import com.sun.source.tree.CompilationUnitTree;
import com.sun.source.tree.ExpressionTree;
import com.sun.source.tree.LineMap;
import com.sun.source.tree.MethodInvocationTree;
import com.sun.source.tree.Scope;
import com.sun.source.tree.StatementTree;
import com.sun.source.tree.Tree;
import com.sun.source.util.JavacTask;
import com.sun.source.util.SourcePositions;
import com.sun.source.util.TreePath;
import com.sun.source.util.TreePathScanner;
import com.sun.source.util.Trees;
import java.io.IOException;
import java.io.PrintStream;
import java.util.List;
import java.util.Optional;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.lang.model.element.AnnotationMirror;
import javax.lang.model.element.Element;
import javax.lang.model.type.TypeMirror;
import javax.lang.model.util.Types;
import javax.tools.JavaFileObject;

/**
 * Parser metedanych i kodu DSL.
 *
 * @author mdrzazga
 */
public class FindDSLVisitor extends TreePathScanner<Void, Void> {

    // offsets of AST nodes in source file 
    private final SourcePositions sourcePositions;
    // bridges Compiler api, Annotation Processing API and Tree API
    private final Trees trees;
    // utility to operate on types
    private final Types types;

    private final PrintStream mout;
   
    private final String[] adnotacje;

    /**
     * Znacznik
     */
    public static final String MAGIC = "BABECAFE";

    private CompilationUnitTree currCompUnit;

    /**
     *
     * @param task
     * @param metaOutput -gdzie pisac metadane jezeli null przyjmujemy
     * System.err
     * @param adnotacje
     */
    public FindDSLVisitor(JavacTask task, PrintStream metaOutput,
            String[] adnotacje) {
        types = task.getTypes();
        trees = Trees.instance(task);
        sourcePositions = trees.getSourcePositions();
        // TAJEMNICZY SCope
        // Scope scope = trees.getScope(null);
        this.adnotacje = adnotacje;

        //  elements = task.getElements();
        if (adnotacje != null && adnotacje.length > 0) {

            System.out.println("processing " + adnotacje[0] + ".... ");

        }

        mout = (metaOutput != null) ? metaOutput : System.err;
       
       

    }

    @Override
    public Void visitMethodInvocation(MethodInvocationTree node, Void p) {
        
        TreePath tp = new TreePath(getCurrentPath(), node);
        Element elem = trees.getElement(tp);
        
        if (isDSLMethodCall(elem)) {
         SortedSet<Long> listaPozycji = getListaPozycji4currentUnit();    
            System.out.println("mamy go w: " + getLineNumber(node) + " " + getPosition(node));

            // wyznaczenie pozycji
            StringBuilder sb = new StringBuilder();
            TypeMirror mirror;
            for (Tree t : getCurrentPath()) {
                //      System.err.print(t.getKind()+"["+trees.getElement(new TreePath(tp, t))+"]/");
                if ((t.getKind() == Tree.Kind.CLASS) || (t.getKind() == Tree.Kind.METHOD)) {
                    Element myMethod = trees.getElement(new TreePath(tp, t));
                    sb.append(myMethod).append("/");
                }
                if (t.getKind() == Tree.Kind.LAMBDA_EXPRESSION) {
                    sb.append(t.getKind() + "/");
                }

            }

            emitPossition("" + elem.getEnclosingElement() + elem.getEnclosedElements() + "." + elem + "@" + sb);
            // wyznacznie argumentow
            List<? extends ExpressionTree> invocationArgs = node.getArguments();
            for (ExpressionTree t : invocationArgs) {
                Element ae = trees.getElement(new TreePath(tp, t));
                emitParam(t, ae);
            }
            // pobranie SScode

            // fileObject ma metody do pobierania strumieni
            TreePath parent = tp.getParentPath();
            StringBuilder parentpath = new StringBuilder();

            while (parent != null) {
                Tree leaf = parent.getLeaf();
                Tree.Kind leafKind = leaf.getKind();
       //           System.out.println("Baza :"+ leafKind+" [POZ: "+sourcePositions.getEndPosition(currCompUnit, leaf));
                listaPozycji.add(sourcePositions.getEndPosition(currCompUnit, leaf));
                // sprwadzamy czy blok
                if (leafKind == Tree.Kind.BLOCK) {
                    //  listaPozycji.add(sourcePositions.getEndPosition(currCompUnit, leaf));
                    BlockTree blocTree = (BlockTree) leaf;
                    
                    for (StatementTree stmt : blocTree.getStatements()) {
          //              System.out.println("w bloku :"+ stmt.getKind()+" [POZ: "+sourcePositions.getStartPosition(currCompUnit, stmt));
                        listaPozycji.add(sourcePositions.getStartPosition(currCompUnit, stmt));
                    }

                }
                // koniec klasy
                if (leafKind == Tree.Kind.CLASS) {
                    //listaPozycji.add(sourcePositions.getEndPosition(currCompUnit, leaf));
                    ClassTree classTree = (ClassTree) leaf;
                    for (Tree stmt : classTree.getMembers()) {
     //                    System.out.println("w klasie :"+ stmt.getKind()+" [POZ: "+sourcePositions.getStartPosition(currCompUnit, stmt));
                        listaPozycji.add(sourcePositions.getStartPosition(currCompUnit, stmt));
                    }

                }
                // BinaryTree tylko pozycja prawego 
                if (leaf instanceof BinaryTree) {
                    BinaryTree binaryTree = (BinaryTree) leaf;
         //         System.out.println("w drzewie :"+ binaryTree.getKind()+" [POZ: "+sourcePositions.getStartPosition(currCompUnit, binaryTree.getRightOperand()));
                    listaPozycji.add(sourcePositions.getStartPosition(currCompUnit, binaryTree.getRightOperand()));
                }

                parentpath.append(parent.getLeaf().getKind() + "[" + getPosition(parent.getLeaf()) + "," + sourcePositions.getEndPosition(currCompUnit, parent.getLeaf()) + "]");

                parent = parent.getParentPath();
            }
        //     System.out.println(parentpath);
            // szukamy 
            JavaFileObject fileObject = currCompUnit.getSourceFile();
         //   System.out.println("URI: " + fileObject.toUri());
            CharSequence simpleText = null;
       //     for(Long lpp: listaPozycji)System.out.print(" "+lpp);
            try {
                int startPos = (int) sourcePositions.getEndPosition(currCompUnit, node);
                Optional<Long> opEndPos = listaPozycji.stream().filter((i) -> (i > startPos)).findFirst();
             //   System.out.println("opEndPos: "+opEndPos.toString());
           //     if(opEndPos.get()!=null)System.out.println(fileObject.getCharContent(true).subSequence(startPos, opEndPos.get().intValue()));
                simpleText = fileObject.getCharContent(true).subSequence(startPos, (opEndPos.get() != null) ? opEndPos.get().intValue() : startPos);
            } catch (IOException ex) {
                Logger.getLogger(FindDSLVisitor.class.getName()).log(Level.SEVERE, null, ex);
            }

            emitMagic(simpleText);

        }
        return super.visitMethodInvocation(node, p);
    }

    @Override
    public Void visitCompilationUnit(CompilationUnitTree tree, Void p) {
        currCompUnit = tree;
        return super.visitCompilationUnit(tree, p);
    }

    private long getLineNumber(Tree tree) {
        // map offsets to line numbers in source file
        LineMap lineMap = currCompUnit.getLineMap();
        if (lineMap == null) {
            return -1;
        }
        // find offset of the specified AST node
        long position = sourcePositions.getStartPosition(currCompUnit, tree);
        return lineMap.getLineNumber(position);
    }

    private long getPosition(Tree tree) {
        // map offsets to line numbers in source file

        long position = sourcePositions.getStartPosition(currCompUnit, tree);
        return position;
    }

    /**
     * eksportuje pozycje wywolania
     *
     * @param metaData
     */
    private void emitPossition(String metaData) {
        mout.println(metaData);
    }

    private void emitParam(ExpressionTree t, Element ae) {
        if (t.getKind() == Tree.Kind.STRING_LITERAL
                || t.getKind() == Tree.Kind.BOOLEAN_LITERAL
                || t.getKind() == Tree.Kind.INT_LITERAL
                || t.getKind() == Tree.Kind.LONG_LITERAL
                || t.getKind() == Tree.Kind.FLOAT_LITERAL
                || t.getKind() == Tree.Kind.DOUBLE_LITERAL
                || t.getKind() == Tree.Kind.CHAR_LITERAL
                || t.getKind() == Tree.Kind.NEW_ARRAY) {
            mout.println(t.getKind() + ":"  + ((ae != null) ? "" + ae + ":" + ae.asType() : t));
        } else {
            mout.println(t.getKind() + ":" + ((ae != null) ? "" + ae + ":" + ae.asType() : ""));
        }
    }

    final static Pattern multiPattern = Pattern.compile(".*/\\*@(.*)\\*/.*", Pattern.MULTILINE | Pattern.DOTALL);
    final static Pattern singlePattern = Pattern.compile("\\s*//@\\{.*\\}");
    final static Pattern magicPattern = Pattern.compile(MAGIC);

    /**
     * emituje zawartosc kodu DSL w komentrzu za wywolaniem. Teks emitoway jest
     * miedzy liniami zawirajacymi znacznik @link{MAGIC}. Jeżeli w kodzie DSL
     * wustepuje ciag znakow MAGIC zostaje zastapiony ciagiem znakow, ktry jest
     * widoczny po znaku @link{MAGIC} i spacji lini znakcznika rozpoczecia kodu
     * DSL.
     * <BR> Tymczasowo parsowany jest tylko pierwszy komentarz multiline
     *
     * @param dslCode
     */
    private void emitMagic(CharSequence dslCode) {
        String kod = null;
        String magicReplacer = MAGIC + "0";
        int magicConflictIter = 0;
        //TODO parsowac zgodnie ze specyfikacja
        if (dslCode != null && dslCode.length() > 0) {
            Matcher mlm = multiPattern.matcher(dslCode);
            if (mlm.matches()) {
                kod = mlm.group(1);
                if (kod.contains(MAGIC)) {
                    magicConflictIter = 1;
               //     System.out.println("dod kontain MAGIC ?");
                    while (kod.contains(magicReplacer)) {
                        magicReplacer = MAGIC + magicConflictIter++;
                    }
                    kod = kod.replaceAll(MAGIC, magicReplacer);
                }

            }
            //  mout.println(dslCode);
        }
       
       
        mout.println((magicConflictIter == 0) ? MAGIC : (MAGIC + " " + magicReplacer));

        if (kod != null) {
            mout.println(kod);
        }
        mout.println(MAGIC);
        
        mout.flush();
        magicConflictIter = 0;
    }

    /**
     * sprawdza czy metoda jest adnotowana przez dowolna adnotacje przkazana
     * konstruktorem
     * </br> smieciowa implemementacja do zmiany
     *
     * @param elem
     * @return
     */
    private boolean isDSLMethodCall(Element elem) {
        //TODO zrobic porawnie z uwzgednieniem pelnej nazwy i listy argumentow
        List<? extends AnnotationMirror> annons = elem.getAnnotationMirrors();
        for (String annot : adnotacje) {
            if (annons.toString().contains(annot)) {
                return true;
            }
        }
        return false;
    }
    /**
     * lista pozcycji per jednostka kompilacji
     * smieciowa implementacja zwraca zawsze nową - zobaczmy czy dziala
     * @return 
     */
    private SortedSet<Long> getListaPozycji4currentUnit() {
       return new TreeSet<>();
    }

}
