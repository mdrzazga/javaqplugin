package javaqplugin;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.LinkOption;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javaqplugin.jasm.EmiterKlasyMetadanych;
import javaqplugin.jasm.MetaInfoParser;
import javaqplugin.jasm.Model;
import javaqplugin.weaver.RabbitHoleWeaver;
import org.openjdk.asmtools.jasm.Main;
;

/**
 *
 * @author mdrzazga
 */
public class MakeDSLTask {

    public static final String METADATA_FILE = "metadata.txt";

    File asmDir, classDir;
    String metaPackage, metaClassName, rabbitHoleName;
    Main compiler ;
    org.openjdk.asmtools.jdis.Main disassembler;
    List<String> dslInokePositions;
    RabbitHoleWeaver weaver;
    
    public void execute() {
        compiler = new Main(System.err, "jasm");
        boolean ok = createMetaDataClass();
        assert ok;
        weaver = new RabbitHoleWeaver();
        weaver.setPositions(dslInokePositions);
        weaver.setRabbitHole(rabbitHoleName);
        ok = disasm();
        assert ok;
        ok = wave();
        assert ok;
        ok = compileModified();
        assert ok;
    }
    
    private boolean compileModified(){
        Path asmDirPath = asmDir.toPath().toAbsolutePath();
        Path classDirPath = classDir.toPath().toAbsolutePath();
        Path path = null, lastPath=null;
        for(String p: dslInokePositions){
            p = weaver.getCompilationUnit(p);
            path = Paths.get(asmDirPath.toString()+"/"+p.replaceAll("\\.", "/") + "_gen.jasm");
            if(!path.equals(lastPath)){
                int lastDot = p.lastIndexOf('.');
               // Path path4class = Paths.get(classDirPath.toString()+"/"+p.substring(0, lastDot).replaceAll("\\.", "/"));
                String argv [] = {"-d", classDirPath.toString(), path.toString()};
            compiler.compile(argv);
            }
            
        }
        return true;
    }
    
     private boolean wave() {
         weaver.setAsmDirectory(asmDir);
         weaver.setDslMeteClassName((metaPackage!=null)?metaPackage+"/"+metaClassName:metaClassName);
         weaver.go();
       return true;
    }
    
    private boolean disasm() {
        Path asmDirPath = asmDir.toPath().toAbsolutePath();
        Path classDirPath = classDir.toPath().toAbsolutePath();
        Path path = null, lastPath=null;
        for(String p: dslInokePositions){
            p = weaver.getCompilationUnit(p);
            path = Paths.get(asmDirPath.toString()+"/"+p.replaceAll("\\.", "/") + ".jasm");
            Path path4class = Paths.get(classDirPath.toString()+"/"+p.replaceAll("\\.", "/") + ".class");
            if(!path.equals(lastPath)){
                try {
                       if (!Files.exists(path, LinkOption.NOFOLLOW_LINKS)){
                           Files.createDirectories(path.getParent());
                           Files.createFile(path);
                       }
                       if(Files.exists(path4class, LinkOption.NOFOLLOW_LINKS)){
                            PrintWriter jdisOut = new PrintWriter(path.toFile());
                            disassembler = new org.openjdk.asmtools.jdis.Main(jdisOut, "jdis");
                            String argv [] = { path4class.toString()};
                            disassembler.disasm(argv);
                            jdisOut.flush();
                            jdisOut.close();
                            
                        }
                } catch (IOException ex) {
                    Logger.getLogger(MakeDSLTask.class.getName()).log(Level.SEVERE, null, ex);
                    return false;
                }
            }
            lastPath=path;
          //  System.out.println("path: "+path);
            
        }
        return true;
    }

    private boolean createMetaDataClass() {
        // load metadata.txt
        List<Model> allModels = new ArrayList<>();
        dslInokePositions = new ArrayList<>();
        try {
            MetaInfoParser mps = new MetaInfoParser(new FileInputStream(new File(asmDir, METADATA_FILE)));
            Model model = mps.next();
            while (model != null) {
                allModels.add(model);
                dslInokePositions.add(model.getPosition());
                model = mps.next();

            }
        } catch (FileNotFoundException ex) {
            Logger.getLogger(MakeDSLTask.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
        //create Metadata asm file path
        String[] metaPath = metaPackage.split("\\.");
        File currDir = asmDir;
        for (String dir : metaPath) {
            currDir = new File(currDir, dir);
            if (!currDir.exists()) {
                if (!currDir.mkdir()) {
                    return false;
                }
            }
        }
        // emit meta class jasm
        EmiterKlasyMetadanych emit = new EmiterKlasyMetadanych(metaPackage, metaClassName, "52:0");
        File asmFile = new File(currDir, metaClassName + "_gen.jasm");
        try {
            emit.createAsmCode(new PrintStream(asmFile), allModels);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(MakeDSLTask.class.getName()).log(Level.SEVERE, null, ex);
        }

        //create Metadata class file path
        currDir = classDir;
        for (String dir : metaPath) {
            currDir = new File(currDir, dir);
            if (!currDir.exists()) {
                if (!currDir.mkdir()) {
                    return false;
                }
            }
        }
        // compile Metadata class file
        String argv [] = {"-d", classDir.getAbsolutePath(), asmFile.getAbsolutePath()};
        compiler.compile(argv);

        return true;
    }

    public void setAsmDir(File asmDir) {
        this.asmDir = asmDir;
    }

    public void setMetaPackage(String metaPackage) {
        this.metaPackage = metaPackage;
    }

    public void setMetaClassName(String metaClassName) {
        this.metaClassName = metaClassName;
    }

    public void setClassDir(File classDir) {
        this.classDir = classDir;
    }

    public void setRabbitHoleName(String rabbitHoleName) {
        this.rabbitHoleName = rabbitHoleName;
    }
    

   

   

}
